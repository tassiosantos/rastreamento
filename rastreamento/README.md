# rastreamento

rastreamento dos correios e envio por e-mail

Para realizar o exercício foi utilizado o PHP, com o gerenciador de pacotes Composer (necessário para instalação da biblioteca mPDF, responsável pela geração do arquivo pdf e PHPMailer, responsável pelo envio do email). 
Foi utilizado o nginx como servidor e o visual studio como editor.
O PHP foi utilizado na versão 7.4, que se apresentou estável para rodar as funcionalidades.

O webservice dos correios, por algum motivo, não envia todas as informações do rastreamento, o que impossibilitou (até a data 19/01/2021), de se preencher todo o corpo do email com as informações requisitadas no exercício, mas a comunicação com o webservice foi realidada com sucesso e os principais dados do rastreamento foram recuperados.

A versão do PHP, inicialmente 8.0, se configurou como uma dificuldade pois não era compatível com a instalação do mPDF, por causa disso, foi substituída pela versão 7.4.
Após algumas configurações do mPDF o seu funcionamento foi concluído e o arquivo pode ser gerado para servir como anexo ao e-mail.

O PHPMailer não foi uma dificuldade muito grande, visto que sua configuração é relativamente simples, mas o gmail (que foi usado para o envio) tem um bloqueio natural nas configurações que, após uma pesquisa, foi ajustado.

As atividades extras não foram realizadas (até a data 19/01) mas seu funcionamento é descrito abaixo:

- O estado de entrega é tabelado no pdf de configuração dos correios(anexado no projeto), e se utilizando das possíveis comparações de strings, seria possível se definir qual texto (entre os diferentes status da encomenda) uma variável de prenchimento de texto receberia, o que seria utilizado em todas as outras funcionalidades, o pdf, o assunto e o título do email.

- Para verificar o status de várias encomendas é necessário apenas se configurar a comunicação SOAP ( com a configuração "L" no campo "tipo") para que ao invés de receber apenas um código de rastreio, se receba vários, sem sequencia e sem caracter de separação.

 - A colocação de uma barra de status no corpo do email (com css), pode ser implementada através do comprimento de um elemento que irá variar de acordo com o status recebido e no pdf, por ser gerado através de um html pelo mPDF, da mesma forma.




Link para configurar o gmail para permitir acessos menos seguros ao e-mail.
https://myaccount.google.com/lesssecureapps




Referência usadas:

Estabelecer conexão com o webservice dos correios:

http://www.visualset.com.br/layouts/importacao_correios_xml_02.pdf

https://github.com/osians/rastreamento-de-pedidos

http://sooho.com.br/2017/03/24/rastreamento-de-pedidos-correios-php-soap/

Criação de pdf:
https://mpdf.github.io/installation-setup/installation-v7-x.html

https://packagist.org/packages/mpdf/mpdf

Envio por e-mail:

https://www.devmedia.com.br/php-envio-de-e-mail-autenticado-utilizando-o-phpmailer/38380


https://www.devmedia.com.br/enviando-e-mail-com-o-phpmailer/9642

https://www.homehost.com.br/blog/tutoriais/php/enviar-email-php-com-phpmailer-smtp/

https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting

https://www.sitepoint.com/sending-emails-php-phpmailer/

https://php.com.br/51?como-enviar-e-mails-com-php-mailer


