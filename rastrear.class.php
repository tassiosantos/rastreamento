<?php
class Rastrear
{     
     # URL de acesso a API dos Correios. 
    private static $wsdl = null ; 

     # Seu nome único de usuário para acesso a API
     # Normalmente obtido na agência de sua cidade
    private static $user = null ; 

     # Sua senha unica de acesso a API dos correios.
     # Deve ser obtida junto ao nome de usuario
    private static $pass = null ; 
     
     # L ou F - Sendo: 
     # L - usado para uma Lista de Objetos; e
     # F - usado para um intervalo de Objetos.
    private static $tipo = null ; 
     
     # Delimita o escopo de resposta de cada objeto.
     # T - Retorna todos os eventos do Objeto 
     # U - Será retornado apenas o último evento do Objeto
    private static $resultado = null ; 

     
     # Deve ser um valor do tipo integer, sendo 
     # 101 - para o retorno em idioma Portugues do Brasil 
     # 102 - para o retorno em idioma Inglês
    private static $idioma = null ; 
    private static $inicializado = false ;
    #Inicia a classe para determinar valores padrões, caso nenhum parâmetro seja enviado.    
    public static function init( $_params = array() )
    {
        self::$wsdl        = isset($_params['wsdl'])      ? $_params['wsdl']      : "http://webservice.correios.com.br/service/rastro/Rastro.wsdl" ; 
        self::$user        = isset($_params['user'])      ? $_params['user']      : "ECT" ;
        self::$pass        = isset($_params['pass'])      ? $_params['pass']      : "SRO" ;
        self::$tipo        = isset($_params['tipo'])      ? $_params['tipo']      : "L" ;
        self::$resultado   = isset($_params['resultado']) ? $_params['resultado'] : "T" ;
        self::$idioma      = isset($_params['idioma'])    ? $_params['idioma']    : "101" ;
        self::$inicializado= true;
    }
    #Função que executa a conexão com o webservice dos correios:
    public static function rastrearObjeto( $__codigo )
{
      $__wsdl = "https://webservice.correios.com.br/service/rastro/Rastro.wsdl";
      $_buscaEventos = array(
        "usuario"   => "ECT",
        "senha"     => "SRO",
        "tipo"      => "L",
        "resultado" => "T",
        "lingua"    => 101
      );
   $_buscaEventos['objetos'] = $__codigo;

   $client = new SoapClient( $__wsdl );
   

   $r = $client->buscaEventosLista($_buscaEventos);
   
   return $r;
}


} 