<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__ . '/vendor/autoload.php';

$assunto = null;
$body = null;


function sendEmail($email=array()){
    
$assunto = $email['assunto'];
$body = $email['body'];
// Inicia a classe PHPMailer 
$mail = new PHPMailer(true); 
// Método de envio 
$mail->IsSMTP(); 
// Usar autenticação SMTP
$mail->SMTPAuth = true;
// Define o host a ser usado para enviar o e-mail
$mail->Host = "smtp.gmail.com"; 
//Define o tipo de segurança
$mail->SMTPSecure = 'ssl';
// Porta utilizada (ssl usa a 465)
$mail->Port = 465; 
// Usuário e senha do servidor SMTP (endereço de email) 

// Email e senha retirados após teste

$mail->Username = ''; 
$mail->Password = ''; 
// Define o remetente 
// Seu e-mail 
$mail->From = "tassiosantos@gmail.com"; 
// Seu nome 
$mail->FromName = "Envio de Tássio Nascimento Santos"; 

// Define o(s) destinatário(s) 
$mail->AddAddress('joao.macedo@elastic.fit', 'João'); 


// Definir se o e-mail é em formato HTML ou texto plano 
// Formato HTML . Use "false" para enviar em formato texto simples ou "true" para HTML.
$mail->IsHTML(true); 

// Charset (opcional) 
$mail->CharSet = 'UTF-8'; 

// Assunto da mensagem 
$mail->Subject = $assunto; 

// Corpo do email 
$mail->Body = $body; 

// Opcional: Anexos 
$mail->AddAttachment("rastreamento.pdf", "rastreamento.pdf"); 

// Envia o e-mail 

$enviado = $mail->Send(); 

// Exibe uma mensagem de resultado 
if ($enviado) 
{ 
    echo "Seu email foi enviado com sucesso! <br>"; 
} else { 
    echo "Houve um erro enviando o email: ".$mail->ErrorInfo; 
} 
}
?>